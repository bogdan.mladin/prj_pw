<?php
    $db_server = "localhost";
    $db_user = "root";
    $db_pass = "";
    $db_name = "hotel_booking";
    $conn = "";

    try{
        $conn = mysqli_connect($db_server,$db_user, $db_pass, $db_name);
    }
    catch(Exception $connectionFailed){
        header("_DB_CONNECTION_ERROR.php");
    }

?>