<?php
    session_start();
    include("database.php");
    $email = $_POST["email"];
    $password = $_POST["password"];
    
    $sql = 
    "SELECT * FROM hotel_booking.users 
    WHERE (EMAIL='$email' AND PASSWORD_='$password')  
    ";
    $result = mysqli_query($conn, $sql);

    if(mysqli_num_rows($result) != 1){
        header("Location: loginError.php");
    }else{
        $row = mysqli_fetch_assoc($result);
        $hashedPassword = $row["PASSWORD_"];
        if(password_verify($password, $hashedPassword) || $password == $row["PASSWORD_"]){
            $_SESSION["username"] = $row["USERNAME"];
            $_SESSION["role_id"] = $row["ROLE_ID"];
            if($_SESSION["role_id"]==2){
                $query =    "SELECT hotels.HOTEL_ID
                            FROM hotels
                            JOIN users ON hotels.USER_ID = users.USER_ID
                            WHERE users.USERNAME = '" . mysqli_real_escape_string($conn,$_SESSION["username"]) . "'";
                $result = mysqli_query($conn,$query);
                if (!$result) {
                    die("Query failed: " . mysqli_error($conn));
                }
                $row = mysqli_fetch_assoc($result);                
                if ($row) {
                    $hotelId = $row['HOTEL_ID'];
                    $_SESSION['hotel_id'] = $hotelId;
                    echo $_SESSION['hotel_id'];
                } else {
                    echo "Username not found or associated with a hotel.";
                }  
            }
            header("Location: homepage.php");
        }else{
            header("Location: loginError.php");
        }
    }
?>