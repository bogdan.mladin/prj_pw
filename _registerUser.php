<?php 
    session_start();
    include("database.php");
    if($conn){
        if($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST["register_data"]))
        {
            $username = filter_input(INPUT_POST,"username",FILTER_SANITIZE_SPECIAL_CHARS);
            $email = filter_input(INPUT_POST,"email",FILTER_SANITIZE_EMAIL);

            $password = $_POST["password"];
            $hashPassword = password_hash($password,PASSWORD_BCRYPT);

            $phoneNumber = preg_replace('/[^0-9]/', '', $_POST['phoneNumber']);

            $name = preg_replace("/[a-zA-Z-]/","",$name);
            $surname = preg_replace("/[a-zA-Z-]/","",$surname);
            $birthday = $_POST["birthday"];

            $insert = "INSERT INTO hotel_booking.users (USERNAME, EMAIL, PASSWORD_, PHONE, NAME_, SURNAME_, BIRTHDAY) 
            VALUES ('$username','$email','$hashPassword','$phoneNumber','$name','$surname','$birthday')";

            try{
                mysqli_query($conn, $insert);
                $_SESSION["username"] = $username;
                $_SESSION["role_id"] = 3;
                header("Location: homepage.php");
                exit();
            }
            catch(Exception $e){
                echo "Registration failed.<br>";
                header("Location: register.ph");
            }
        }

    }
?>