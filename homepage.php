<?php
session_start();
include("database.php");
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BooKings</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/_headerHomepage.css">
    <link rel="stylesheet" href="css/_mainBodyHomepage.css">
    <link rel="stylesheet" href="css/_footerHomepage.css">
    <link rel="stylesheet" href="css/_CARD.css">   
</head>
<body>  
    <header>
        <nav>
            <label class="logo">BooKings👻👑</label>
            <ul class="nav_links">
                <!--<li><a href="#">HOME</a></li>-->

                <!-- TO DO: CREATE USER MENU, HOTEL MENU, ADMIN MENU -->
                <?php
                if(isset($_SESSION["username"]) && $_SESSION['role_id'] == 3)
                    echo '<li><a href="menus\userMenu\userMenu.html">MENU</a></li>';
                elseif(isset($_SESSION['username']) && $_SESSION['role_id'] == 2)
                    echo '<li><a href="menus/hotelMenu/hotelMenu.html">MENU</a></li>';
                elseif(isset($_SESSION['username']) && $_SESSION['role_id'] == 1)
                    echo '<li><a href="menus/adminMenu/adminMenu.html">MENU</a></li>';
                ?>

                <li><a href="#">ABOUT</a></li>
                <li><a href="#">HELP</a></li>
                <li><a href="#">FEEDBACK</a></li>
            </ul>
            <?php
            if(!isset($_SESSION["username"]))
                echo '<a href="login.php"><button class="connStBtn">CONNECT</button></a>';
            else
                echo '<a href="logoutButton.php"><button class="connStBtn">LOGOUT</button></a>';
            ?>
        </nav>
    </header>

    <main class="mainBody">
        <div class="roomCards">
        <?php 
        $sql = "SELECT rooms.*, hotels.HOTEL_NAME FROM rooms
                JOIN hotels ON rooms.HOTEL_ID = hotels.HOTEL_ID
                ORDER BY FIELD(rooms.booking_status, 'available', 'unavailable'), hotels.HOTEL_NAME";
        $result = mysqli_query($conn, $sql);

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $roomId = $row["ROOM_ID"];
                $hotelID = $row["HOTEL_ID"];
                $hotelName = $row["HOTEL_NAME"]; 
                $roomImage = $row["ROOM_IMAGE"]; 
                $price = $row["PRICE_PER_NIGHT"];
                $status = ($row["BOOKING_STATUS"] == "available") ? "Available" : "Unavailable";
                if($status == 'Available'){    
                    echo '<div class="card">';
                    echo '<img src="menus/hotelMenu/roomImages/IMG-' . $hotelID . '-' . $roomId . '.jpg" class="card_image">';
                    echo '<div class="card_content">';
                    echo '<h2 class="card_title">' . $hotelName . '</h2>';
                    echo '<pre>Price: $' . $price . '<br>Status: ' . $status . '</pre>';
                    
                    if (isset($_SESSION["username"])) {
                        echo '<form method="post" action="book_room.php" class="booking-form">';
                        echo '<input type="hidden" name="hotel_id" value="' . $hotelID . '">';
                        echo '<input type="hidden" name="room_id" value="' . $roomId . '">';
                        echo '<label for="arrival_date">Check-In Date:</label>  ';
                        echo '<input type="date" name="arrival_date" required>';
                        echo '<label for="leave_date">Check-Out Date:</label>';
                        echo '<input type="date" name="leave_date" required>';
                        echo '<input type="submit" name="submit" value="BOOK ROOM">';
                        echo '</form>';
                    }

                    echo '</div>';
                    echo '</div>';
                }
            }
        } else {
            echo "No rooms available at the moment.";
        }
        ?>
        </div>
    </main>

    <footer></footer>
</body>
</html>
