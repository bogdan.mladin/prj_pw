<?php

session_start();
include("database.php");

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_SESSION["username"])) {
    $stmt = $conn->prepare("SELECT USER_ID FROM users WHERE USERNAME = ?");
    $stmt->bind_param("s", $_SESSION["username"]);
    $stmt->execute();
    $result = $stmt->get_result();
    $user = $result->fetch_assoc();
    
    if ($user) {
        $userId = $user['USER_ID']; 
        $hotelId = $_POST["hotel_id"]; 
        $roomId = $_POST["room_id"]; 
        $arrivalDate = $_POST["arrival_date"];
        $leaveDate = $_POST["leave_date"]; 

        $date1 = new DateTime($arrivalDate);
        $date2 = new DateTime($leaveDate);
        $diff = $date1->diff($date2);
        $noOfNights = $diff->days;

  
        $conn->begin_transaction();

       
        $stmt = $conn->prepare("INSERT INTO booking (USER_ID, HOTEL_ID, ROOM_ID, ARRIVAL_DATE, LEAVE_DATE, NO_OF_NIGHTS) VALUES (?, ?, ?, ?, ?, ?)");
        
        $stmt->bind_param("iiissi", $userId, $hotelId, $roomId, $arrivalDate, $leaveDate, $noOfNights);

        if ($stmt->execute()) {
            $updateStmt = $conn->prepare("UPDATE rooms SET BOOKING_STATUS = 'unavailable' WHERE ROOM_ID = ? AND HOTEL_ID = ?");
            $updateStmt->bind_param("ii", $roomId, $hotelId);
            if($updateStmt->execute()) {
                $conn->commit();
                echo "Booking successful! You have booked for " . $noOfNights . " nights.";
            } else {
                $conn->rollback();
                echo "Booking successful, but failed to update the room status to unavailable.";
            }
            $updateStmt->close();
        } else {
            echo "Error: " . $stmt->error;
        }
        
        $stmt->close();
    } else {
        echo "User not found.";
    }
} else {
    echo "You must be logged in to book a room.";
}

$conn->close();
?>
