<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Room Registration</title>
    <link rel="stylesheet" href="../../css/register.css">
</head>
<body>
<register>
    <h2>ROOM REGISTRATION</h2>
    <h1>BooKings👻👑</h1>
    <form method="POST" action="_registerRoom.php" enctype="multipart/form-data">
        <input type="text" name="room_number" placeholder="Room Number" required class="formInput">
        <input type="text" name="price_per_night" placeholder="Price Per Night" required class="formInput">
        <select name="booking_status" class="formInput" required>
            <option value="available">Available</option>
            <option value="unavailable">Unavailable</option>
        </select>
        <input type="file" name="room_image" required class="formInput">
        <button type="submit" name="register_data" value="Upload" class="buttonRegister">SUBMIT</button>
    </form>
</register>
</body> 
</html>
