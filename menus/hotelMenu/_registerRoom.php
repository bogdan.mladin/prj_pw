<?php
session_start();

if (!isset($_SESSION["hotel_id"], $_SESSION["username"])) {
    header("Location: login.php?error=Please log in first");
    exit;
}

include('../../database.php');

if (isset($_POST['register_data']) && isset($_FILES['room_image'])) {
    
    $hotelId = $_SESSION['hotel_id'];
    $roomNumber = $_POST['room_number']; 
    $pricePerNight = $_POST['price_per_night']; 
    $bookingStatus = $_POST['booking_status']; 

    $query = "SELECT MAX(ROOM_ID) AS max_room_id FROM rooms";
    $result = mysqli_query($conn, $query);
    $row = mysqli_fetch_assoc($result);
    $largestRoomId = $row['max_room_id'];
    $largestRoomId++;
    
    $roomImg_name = $_FILES['room_image']['name'];
    $roomImg_size = $_FILES['room_image']['size'];
    $roomImg_tmpName = $_FILES['room_image']['tmp_name'];
    $roomImg_error = $_FILES['room_image']['error'];

    if ($roomImg_error === 0) {
        if ($roomImg_size > 5 * 1024 * 1024) {
            $em = "Error: File size is too large. Max limit is 5MB.";
            header("Location: addRoom.php?error=$em");
            exit;
        } else {
            $roomImg_ex = pathinfo($roomImg_name, PATHINFO_EXTENSION);
            $roomImg_ex_lc = strtolower($roomImg_ex);
            $allowed_exs = array("jpg", "jpeg");

            if (in_array($roomImg_ex_lc, $allowed_exs)) {
                $newRoomImg_name = "IMG-" . $hotelId . "-" . $largestRoomId . '.' . $roomImg_ex_lc;
                $roomImg_uploadPath = 'roomImages/' . $newRoomImg_name;
                
                if (move_uploaded_file($roomImg_tmpName, $roomImg_uploadPath)) {
                    $stmt = $conn->prepare("INSERT INTO rooms (HOTEL_ID, ROOM_NUMBER, PRICE_PER_NIGHT, BOOKING_STATUS) VALUES (?, ?, ?, ?)");
                    $stmt->bind_param("iids", $hotelId, $roomNumber, $pricePerNight, $bookingStatus);

                    if ($stmt->execute()) {
                        header("Location: roomList.php?success=Room added successfully");
                        exit;
                    } else {
                        unlink($roomImg_uploadPath);
                        $em = "Database error: Failed to insert room.";
                        header("Location: addRoom.php?error=$em");
                        exit;
                    }
                } else {
                    $em = "Error: Could not move the uploaded file.";
                    header("Location: addRoom.php?error=$em");
                    exit;
                }
            } else {
                $em = "Error: File format not accepted. Only JPG and JPEG files are allowed.";
                header("Location: addRoom.php?error=$em");
                exit;
            }
        }
    } else {
        $em = "Error: An error occurred during file upload.";
        header("Location: addRoom.php?error=$em");
        exit;
    }
} else {
    header("Location: addRoom.php");
    exit;
}
?>
