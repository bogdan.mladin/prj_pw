<?php
session_start();
include("../../database.php");
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BooKings</title>
    <link rel="stylesheet" href="../../css/header.css">
    <link rel="stylesheet" href="..\..\css\_tableStyling.css">
</head>
<body>  
    <header>
        <nav>
            <label class="logo">BooKings👻👑</label>
            <ul class="nav_links">
            <li><a href="../../homepage.php">HOME</a></li>

                

                <li><a href="#">ABOUT</a></li>
                <li><a href="#">HELP</a></li>
                <li><a href="#">FEEDBACK</a></li>
            </ul>
            <?php
            if(!isset($_SESSION["username"]))
                echo '<a href="../../login.php"><button class="connStBtn">CONNECT</button></a>';
            else
                echo '<a href="../../logoutButton.php"><button class="connStBtn">LOGOUT</button></a>';
            ?>
        </nav>
    </header>
</html>

<?php
include("../../database.php");

$hotel_id = $_SESSION["hotel_id"];

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["room_id"])) {
        $room_id = $_POST["room_id"];
        
        if (isset($_POST["booking_status"])) {
            // Update status
            $booking_status = $_POST["booking_status"];
            $updateSql = "UPDATE rooms SET BOOKING_STATUS = '$booking_status' WHERE ROOM_ID = $room_id";
            if ($conn->query($updateSql) === TRUE) {
                echo "Booking status updated successfully!";
            } else {
                echo "Error updating booking status: " . $conn->error;
            }
        } elseif (isset($_POST["delete"])) {
            // Delete room
            $deleteSql = "DELETE FROM rooms WHERE ROOM_ID = $room_id";
            if ($conn->query($deleteSql) === TRUE) {
                echo "Room deleted successfully!";
            } else {
                echo "Error deleting room: " . $conn->error;
            }
        }
    }
}

$sql = "SELECT ROOM_ID, HOTEL_ID, ROOM_NUMBER, PRICE_PER_NIGHT, BOOKING_STATUS FROM rooms WHERE HOTEL_ID = $hotel_id";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<table border='1'>
        <tr>
            <th>ROOM_ID</th>
            <th>HOTEL_ID</th>
            <th>ROOM_NUMBER</th>
            <th>PRICE_PER_NIGHT</th>
            <th>BOOKING_STATUS</th>
            <th>Actions</th>
        </tr>";

    while ($row = $result->fetch_assoc()) {
        echo "<tr>
            <td>" . $row["ROOM_ID"] . "</td>
            <td>" . $row["HOTEL_ID"] . "</td>
            <td>" . $row["ROOM_NUMBER"] . "</td>
            <td>" . $row["PRICE_PER_NIGHT"] . "</td>
            <td>" . $row["BOOKING_STATUS"] . "</td>
            <td>
                <form method='post'>
                    <input type='hidden' name='room_id' value='" . $row["ROOM_ID"] . "'>
                    <select name='booking_status'>
                        <option value='available' " . ($row["BOOKING_STATUS"] == 'available' ? 'selected' : '') . ">Available</option>
                        <option value='unavailable' " . ($row["BOOKING_STATUS"] == 'unavailable' ? 'selected' : '') . ">Unavailable</option>
                    </select>
                    <input type='submit' value='Update Status'>
                </form>
                <form method='post'>
                    <input type='hidden' name='room_id' value='" . $row["ROOM_ID"] . "'>
                    <input type='hidden' name='delete' value='true'>
                    <input type='submit' value='DELETE'>
                </form>
            </td>
        </tr>";
    }
    echo "</table>";
} else {
    echo "No records found in the rooms table.";
}

$conn->close();
?>

</body>
</html>
