<?php
session_start();
include("../../database.php");
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BooKings</title>
    <link rel="stylesheet" href="../../css/header.css">
    <link rel="stylesheet" href="..\..\css\_tableStyling.css">
</head>
<body>  
    <header>
        <nav>
            <label class="logo">BooKings👻👑</label>
            <ul class="nav_links">
            <li><a href="../../homepage.php">HOME</a></li>

                

                <li><a href="#">ABOUT</a></li>
                <li><a href="#">HELP</a></li>
                <li><a href="#">FEEDBACK</a></li>
            </ul>
            <?php
            if(!isset($_SESSION["username"]))
                echo '<a href="../../login.php"><button class="connStBtn">CONNECT</button></a>';
            else
                echo '<a href="../../logoutButton.php"><button class="connStBtn">LOGOUT</button></a>';
            ?>
        </nav>
    </header>
</html>
<?php

include("../../database.php");
if (isset($_SESSION["hotel_id"])) {
    $hotel_id = $_SESSION["hotel_id"];

    if ($conn === false) {
        die("ERROR: Could not connect. " . mysqli_connect_error());
    }

    $sql = "SELECT * FROM booking WHERE HOTEL_ID = ?";
    
    if ($stmt = mysqli_prepare($conn, $sql)) {
        mysqli_stmt_bind_param($stmt, "i", $hotel_id);
        if(mysqli_stmt_execute($stmt)) {
            $result = mysqli_stmt_get_result($stmt);

            if(mysqli_num_rows($result) > 0) {
                echo "<table border='1'>";
                echo "<tr>
                    <th>USER_ID</th>
                    <th>HOTEL_ID</th>
                    <th>ROOM_ID</th>
                    <th>ARRIVAL_DATE</th>
                    <th>LEAVE_DATE</th>
                    <th>NO_OF_NIGHTS</th>
                </tr>";
                
                while($booking = mysqli_fetch_assoc($result)) {
                    echo "<tr>";
                    echo "<td>" . htmlspecialchars($booking['USER_ID']) . "</td>";
                    echo "<td>" . htmlspecialchars($booking['HOTEL_ID']) . "</td>";
                    echo "<td>" . htmlspecialchars($booking['ROOM_ID']) . "</td>";
                    echo "<td>" . htmlspecialchars($booking['ARRIVAL_DATE']) . "</td>";
                    echo "<td>" . htmlspecialchars($booking['LEAVE_DATE']) . "</td>";
                    echo "<td>" . htmlspecialchars($booking['NO_OF_NIGHTS']) . "</td>";
                    echo "</tr>";
                }
                echo "</table>";

                mysqli_free_result($result);
            } else {
                echo "No bookings found for the selected hotel.";
            }
        } else {
            echo "ERROR: Could not execute $sql. " . mysqli_error($conn);
        }

        mysqli_stmt_close($stmt);
    } else {
        echo "ERROR: Could not prepare the query: $sql. " . mysqli_error($conn);
    }

    mysqli_close($conn);
} else {
    echo "Hotel ID is not set in the session.";
}
?>
