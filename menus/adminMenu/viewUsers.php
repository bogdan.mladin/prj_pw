<?php
session_start();
include("../../database.php");
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BooKings</title>
    <link rel="stylesheet" href="../../css/header.css">
    <link rel="stylesheet" href="..\..\css\_tableStyling.css">
</head>
<body>  
    <header>
        <nav>
            <label class="logo">BooKings👻👑</label>
            <ul class="nav_links">
                <li><a href="../../homepage.php">HOME</a></li>

                <!-- TO DO: CREATE USER MENU, HOTEL MENU, ADMIN MENU -->
                

                <li><a href="#">ABOUT</a></li>
                <li><a href="#">HELP</a></li>
                <li><a href="#">FEEDBACK</a></li>
            </ul>
            <?php
            if(!isset($_SESSION["username"]))
                echo '<a href="../../login.php"><button class="connStBtn">CONNECT</button></a>';
            else
                echo '<a href="../../logoutButton.php"><button class="connStBtn">LOGOUT</button></a>';
            ?>
        </nav>
    </header>
</html>

<?php
include("../../database.php");

function deleteUser($userId, $roleId, $conn) {
    $conn->begin_transaction();

    try {
        if ($roleId == 2) {
            $conn->query("DELETE FROM booking WHERE HOTEL_ID IN (SELECT HOTEL_ID FROM rooms WHERE ROOM_ID = $userId)");

            $conn->query("DELETE FROM rooms WHERE HOTEL_ID = $userId");
        }

        $conn->query("DELETE FROM users WHERE USER_ID = $userId");

        $conn->commit();
    } catch (Exception $e) {
        $conn->rollback();
    }
}

if (isset($_GET['delete']) && isset($_GET['role'])) {
    $userId = (int)$_GET['delete'];
    $roleId = (int)$_GET['role'];
    deleteUser($userId, $roleId, $conn);
}

$result = $conn->query("SELECT * FROM users ORDER BY ROLE_ID");

if (!$result) {
    die("Error retrieving users: " . $conn->error);
}

echo "<table border='1'>";
echo "<tr><th>Username</th><th>Email</th><th>Name</th><th>Surname</th><th>Role ID</th><th>Actions</th></tr>";
while ($row = $result->fetch_assoc()) {
    echo "<tr>";
    echo "<td>" . htmlspecialchars($row['USERNAME']) . "</td>";
    echo "<td>" . htmlspecialchars($row['EMAIL']) . "</td>";
    echo "<td>" . htmlspecialchars($row['NAME_']) . "</td>";
    echo "<td>" . htmlspecialchars($row['SURNAME_']) . "</td>";
    echo "<td>" . $row['ROLE_ID'] . "</td>";
    echo "<td>";
    if ($row['ROLE_ID'] != 1) {
        echo "<a href='?delete=" . $row['USER_ID'] . "&role=" . $row['ROLE_ID'] . "' onclick='return confirm(\"Are you sure you want to delete this user?\");'>Delete</a>";
    }
    echo "</td>";
    echo "</tr>";
}
echo "</table>";

$conn->close();
?>
