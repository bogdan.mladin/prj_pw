<?php
session_start();

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['register_data'])) {
    include("../../database.php");
    $username = mysqli_real_escape_string($conn, $_POST['username']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $password = mysqli_real_escape_string($conn, $_POST['password']); 
    $phoneNumber = mysqli_real_escape_string($conn, $_POST['phoneNumber']);
    $name = mysqli_real_escape_string($conn, $_POST['name']);
    $surname = mysqli_real_escape_string($conn, $_POST['surname']);
    $birthday = $_POST['birthday'];

    $hashed_password = password_hash($password, PASSWORD_DEFAULT);

    $query = "INSERT INTO users (ROLE_ID, USERNAME, EMAIL, PASSWORD_, PHONE, NAME_, SURNAME_, BIRTHDAY) VALUES (2, '$username', '$email', '$hashed_password', '$phoneNumber', '$name', '$surname', '$birthday')";

    if (mysqli_query($conn, $query)) {
        echo "New hotel user created successfully";
    } else {
        echo "Error: " . $query . "<br>" . mysqli_error($conn);
    }

    mysqli_close($conn);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BooKings Register</title>
    <link rel="stylesheet" href="../../css/register.css">
</head>
<body>
<register>
    <h2>CREATE HOTEL</h2>
    <h1>BooKings👻👑</h1>
    <form method="POST">
        <input type="text" name="username" placeholder="Username" required class="formInput">
        <input type="email" name="email" placeholder="user@BooKings.com" required class="formInput">
        <input type="password" name="password" placeholder="Password" required class="formInput">
        <input type="tel" name="phoneNumber" placeholder="Phone number: 111-111-1111" required class="formInput">
        <div class="fullName">
            <input type="text" name="name" placeholder="Name" required id="name">
            <input type="text" name="surname" placeholder="Surname" required id="surname">
        </div>
        <input type="date" name="birthday" required class="formInput">
        <button type="submit" name="register_data" class="buttonRegister">SUBMIT</button>
    </form>
</register>
</body>
</html>
