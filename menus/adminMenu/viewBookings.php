<?php
session_start();
include("../../database.php");
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BooKings</title>
    <link rel="stylesheet" href="../../css/header.css">
    <link rel="stylesheet" href="..\..\css\_tableStyling.css">
</head>
<body>  
    <header>
        <nav>
            <label class="logo">BooKings👻👑</label>
            <ul class="nav_links">
            <li><a href="../../homepage.php">HOME</a></li>
                <!-- TO DO: CREATE USER MENU, HOTEL MENU, ADMIN MENU -->
                
                <li><a href="#">ABOUT</a></li>
                <li><a href="#">HELP</a></li>
                <li><a href="#">FEEDBACK</a></li>
            </ul>
            <?php
            if(!isset($_SESSION["username"]))
                echo '<a href="../../login.php"><button class="connStBtn">CONNECT</button></a>';
            else
                echo '<a href="../../logoutButton.php"><button class="connStBtn">LOGOUT</button></a>';
            ?>
        </nav>
    </header>
</html>

<?php
include("../../database.php");

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['delete'])) {
    $userId = mysqli_real_escape_string($conn, $_POST['user_id']);
    $hotelId = mysqli_real_escape_string($conn, $_POST['hotel_id']);
    $roomId = mysqli_real_escape_string($conn, $_POST['room_id']);

    $stmt = mysqli_prepare($conn, "DELETE FROM booking WHERE USER_ID=? AND HOTEL_ID=? AND ROOM_ID=?");
    mysqli_stmt_bind_param($stmt, 'iii', $userId, $hotelId, $roomId);

    if (mysqli_stmt_execute($stmt)) {
        echo "<p>Record deleted successfully.</p>";
    } else {
        echo "<p>Error deleting record: " . mysqli_error($conn) . "</p>";
    }

    mysqli_stmt_close($stmt);
}

$query = "SELECT * FROM booking";
$result = mysqli_query($conn, $query);

if (!$result) {
    die("Query failed: " . mysqli_error($conn));
}

echo "<table border='1'>";
echo "<tr>
        <th>User ID</th>
        <th>Hotel ID</th>
        <th>Room ID</th>
        <th>Arrival Date</th>
        <th>Leave Date</th>
        <th>No of Nights</th>
        <th>Action</th>
      </tr>";

while ($row = mysqli_fetch_assoc($result)) {
    echo "<tr>";
    echo "<td>" . htmlspecialchars($row['USER_ID']) . "</td>";
    echo "<td>" . htmlspecialchars($row['HOTEL_ID']) . "</td>";
    echo "<td>" . htmlspecialchars($row['ROOM_ID']) . "</td>";
    echo "<td>" . htmlspecialchars($row['ARRIVAL_DATE']) . "</td>";
    echo "<td>" . htmlspecialchars($row['LEAVE_DATE']) . "</td>";
    echo "<td>" . htmlspecialchars($row['NO_OF_NIGHTS']) . "</td>";
    
    echo "<td>
            <form method='POST' action=''>
                <input type='hidden' name='user_id' value='" . $row['USER_ID'] . "'>
                <input type='hidden' name='hotel_id' value='" . $row['HOTEL_ID'] . "'>
                <input type='hidden' name='room_id' value='" . $row['ROOM_ID'] . "'>
                <input type='submit' name='delete' value='Delete'>
            </form>
          </td>";
    
    echo "</tr>";
}
echo "</table>";

mysqli_free_result($result);
mysqli_close($conn);
?>
