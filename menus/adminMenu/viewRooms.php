<?php
session_start();
include("../../database.php");
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BooKings</title>
    <link rel="stylesheet" href="../../css/header.css">
    <link rel="stylesheet" href="..\..\css\_tableStyling.css">
</head>
<body>  
    <header>
        <nav>
            <label class="logo">BooKings👻👑</label>
            <ul class="nav_links">
            <li><a href="../../homepage.php">HOME</a></li>
            
                <!-- TO DO: CREATE USER MENU, HOTEL MENU, ADMIN MENU -->
                

                <li><a href="#">ABOUT</a></li>
                <li><a href="#">HELP</a></li>
                <li><a href="#">FEEDBACK</a></li>
            </ul>
            <?php
            if(!isset($_SESSION["username"]))
                echo '<a href="../../login.php"><button class="connStBtn">CONNECT</button></a>';
            else
                echo '<a href="../../logoutButton.php"><button class="connStBtn">LOGOUT</button></a>';
            ?>
        </nav>
    </header>
</html>

<?php
include("../../database.php");

if (isset($_GET['deleteRoom'])) {
    $roomId = (int)$_GET['deleteRoom'];
    
    $deleteQuery = "DELETE FROM rooms WHERE ROOM_ID = $roomId";
    if ($conn->query($deleteQuery) === TRUE) {
        echo "Room deleted successfully.";
    } else {
        echo "Error deleting room: " . $conn->error;
    }
}

$result = $conn->query("SELECT * FROM rooms");

if (!$result) {
    die("Error retrieving rooms: " . $conn->error);
}

echo "<table border='1' class= 'content-table'>";
echo "<tr><th>Room ID</th><th>Hotel ID</th><th>Room Number</th><th>Price Per Night</th><th>Booking Status</th><th>Actions</th></tr>";
while ($row = $result->fetch_assoc()) {
    echo "<tr>";
    echo "<td>" . $row['ROOM_ID'] . "</td>";
    echo "<td>" . $row['HOTEL_ID'] . "</td>";
    echo "<td>" . $row['ROOM_NUMBER'] . "</td>";
    echo "<td>" . $row['PRICE_PER_NIGHT'] . "</td>";
    echo "<td>" . $row['BOOKING_STATUS'] . "</td>";
    echo "<td>";
    echo "<a href='?deleteRoom=" . $row['ROOM_ID'] . "' onclick='return confirm(\"Are you sure you want to delete this room?\");'>Delete</a>";
    echo "</td>";
    echo "</tr>";
}
echo "</table>";
$conn->close();
?>
