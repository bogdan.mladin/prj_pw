<?php
session_start();
include("../../database.php");
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BooKings</title>
    <link rel="stylesheet" href="../../css/header.css">
    <link rel="stylesheet" href="..\..\css\_tableStyling.css">
</head>
<body>  
    <header>
        <nav>
            <label class="logo">BooKings👻👑</label>
            <ul class="nav_links">
            <li><a href="../../homepage.php">HOME</a></li>

                

                <li><a href="#">ABOUT</a></li>
                <li><a href="#">HELP</a></li>
                <li><a href="#">FEEDBACK</a></li>
            </ul>
            <?php
            if(!isset($_SESSION["username"]))
                echo '<a href="../../login.php"><button class="connStBtn">CONNECT</button></a>';
            else
                echo '<a href="../../logoutButton.php"><button class="connStBtn">LOGOUT</button></a>';
            ?>
        </nav>
    </header>
</html> 

<?php

include("../../database.php");

if (isset($_SESSION["username"])) {
    $username = $_SESSION["username"];

    if ($conn === false) {
        die("ERROR: Could not connect. " . mysqli_connect_error());
    }

    $sql_user = "SELECT USER_ID FROM users WHERE USERNAME = ?";
    
    if ($stmt_user = mysqli_prepare($conn, $sql_user)) {
        mysqli_stmt_bind_param($stmt_user, "s", $username);
        if(mysqli_stmt_execute($stmt_user)) {
            $result_user = mysqli_stmt_get_result($stmt_user);
            if($user = mysqli_fetch_assoc($result_user)) {
                $user_id = $user['USER_ID'];

                $sql = "SELECT * FROM booking WHERE USER_ID = ?";
                
                if ($stmt = mysqli_prepare($conn, $sql)) {
                    mysqli_stmt_bind_param($stmt, "i", $user_id);
                    if(mysqli_stmt_execute($stmt)) {
                        $result = mysqli_stmt_get_result($stmt);

                        if(mysqli_num_rows($result) > 0) {
                            echo "<table border='1'>";
                            echo "<tr>
                                <th>USER_ID</th>
                                <th>HOTEL_ID</th>
                                <th>ROOM_ID</th>
                                <th>ARRIVAL_DATE</th>
                                <th>LEAVE_DATE</th>
                                <th>NO_OF_NIGHTS</th>
                            </tr>";
                            
                            while($booking = mysqli_fetch_assoc($result)) {
                                echo "<tr>";
                                echo "<td>" . htmlspecialchars($booking['USER_ID']) . "</td>";
                                echo "<td>" . htmlspecialchars($booking['HOTEL_ID']) . "</td>";
                                echo "<td>" . htmlspecialchars($booking['ROOM_ID']) . "</td>";
                                echo "<td>" . htmlspecialchars($booking['ARRIVAL_DATE']) . "</td>";
                                echo "<td>" . htmlspecialchars($booking['LEAVE_DATE']) . "</td>";
                                echo "<td>" . htmlspecialchars($booking['NO_OF_NIGHTS']) . "</td>";
                                echo "</tr>";
                            }
                            echo "</table>";

                            mysqli_free_result($result);
                        } else {
                            echo "No bookings found.";
                        }
                    } else {
                        echo "ERROR: Could not execute $sql. " . mysqli_error($conn);
                    }

                    mysqli_stmt_close($stmt);
                } else {
                    echo "ERROR: Could not prepare the query: $sql. " . mysqli_error($conn);
                }
                mysqli_free_result($result_user);
            } else {
                echo "User not found.";
            }
        } else {
            echo "ERROR: Could not execute $sql_user. " . mysqli_error($conn);
        }

        mysqli_stmt_close($stmt_user);
    } else {
        echo "ERROR: Could not prepare the user query: $sql_user. " . mysqli_error($conn);
    }

    mysqli_close($conn);
} else {
    echo "You must be logged in to view your bookings.";
}
?>
